<?php

namespace CleanPhp\\Invoice\\Domain\\Entity;

class Customer extends AbstractEntity {
    protected $name;
    protected $email;

    public function getName(): String{
        return $this->name;
    }

    public function getEmail(): String {
        return $this->email;
    }

    public function setName(String $name){
        $this->name = $name
        return $this;
    }

    public function setEmail(String $email){
        $this->email = $email;
        return $this;
    }

}
