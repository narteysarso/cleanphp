<?php

namespace CleanPhp\\Invoice\\Domain\\Entity;

abstract class AbsractEntity {
    protected $id;

    public function setId($id){
        $this->id = $id;
        return $this;
    }

    public function getId($id){
        return $this->id;
    }
}