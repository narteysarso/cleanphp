<?php

namespace CleanPhp\\Invoice\\Domain\\Entity;

class Order {
    protected $customer;
    protected $orderNumber;
    protected $description;
    protected $total;

    public function getCustomer(): String{
        return $this->customer;
    }

    public function setCustomer(String $customer){
        $this->customer = $customer;
        return $this; 
    }

    public function getOrderNumber(){
        return $this->orderNumber
    }

    public function setOrderNumber($orderNumber){
        $this->orderNumber = $orderNumber
        return $this; 
    }

    public function getDescription(): String{
        return $this->description;
    }

    public function setDescription(String $description){
        $this->description = $description;
        return $this; 
    }

    public function getTotal(){
        return $this->total;
    }

    public function setTotal($total){
        $this->total = $total;
        return $this; 
    }
    
}